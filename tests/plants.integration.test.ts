import app from '../src/app';
import mongoose, { ObjectId } from 'mongoose';
import request from 'supertest';
import Plant, { PlantInterface } from '../src/models/plant.model';
import { PlantsRepository } from '../src/plants/plants.repository';
import { container } from 'tsyringe';

describe('plantsModule', () => {
  const url = process.env.MONGO + '';
  const plantId = new mongoose.Types.ObjectId();
  const wrongId = new mongoose.Types.ObjectId();

  const plantMock = {
    _id: plantId,
    name: 'name',
    wateringFrequency: 'water',
    light: 'not much',
    thirsty: false,
  };

  beforeAll(async () => {
    await mongoose.disconnect();
    await mongoose.connect(url, {});
  });

  beforeEach(async () => {
    await Plant.deleteMany();
    jest.restoreAllMocks();
  });

  afterAll(async () => {
    await Plant.deleteMany();
    await mongoose.disconnect();
  });

  test('create plant success 201', async () => {
    const plantCountBefore = await Plant.countDocuments();
    const response = await request(app).post('/api/plants/create').send(plantMock).expect(201);
    expect(response.body.message).toBe('plant created');

    const periodCountAfter = await Plant.countDocuments();
    expect(plantCountBefore + 1).toBe(periodCountAfter);
  });

  test('create plant error 500', async () => {
    await new Plant(plantMock).save();
    let plant = await Plant.findById(plantMock._id);
    expect(plant).not.toBeNull();

    const response = await request(app).post('/api/plants/create').send(plant).expect(500);
    expect(response.body.message).toBe('creating plant failed');
  });

  test('getAll success 200', async () => {
    await new Plant(plantMock).save();
    let plant = await Plant.findById(plantId);
    expect(plant).not.toBeNull();
    const response = await request(app)
      .get('/api/plants/getAll')
      // .set({
      //   Authorization: `Bearer ${userOne.tokens[0].token}`,
      //   userData: {
      //     userId: userId,
      //     email: 'user@controller.com',
      //     username: 'user',
      //   },
      // })
      .send()
      .expect(200);
    expect(response.body.message).toBe('plants fetched');
    expect(response.body.plants.length).toBe(1);
  });

  test('getAll error 500', async () => {
    type PlantDocument = Document & PlantInterface;
    class MockedPlantsRepository extends PlantsRepository {
      // @ts-ignore
      async findAll(): Promise<PlantDocument[]> {
        throw new Error('Mocked findAll error');
      }
    }
    container.register<PlantsRepository>(PlantsRepository, {
      // @ts-ignore
      useValue: new MockedPlantsRepository(),
    });

    jest.spyOn(container.resolve(PlantsRepository), 'findAll');
    await new Plant(plantMock).save();
    let plant = await Plant.findById(plantId);
    expect(plant).not.toBeNull();
    const response = await request(app)
      .get('/api/plants/getAll')
      // .set({
      //   Authorization: `Bearer ${userOne.tokens[0].token}`,
      //   userData: {
      //     userId: userId,
      //     email: 'user@controller.com',
      //     username: 'user',
      //   },
      // })
      .send()
      .expect(500);
    expect(response.body.message).toBe('fetching plants failed');
    expect(container.resolve(PlantsRepository).findAll).toHaveBeenCalled();
  });

  test('update plant success 200', async () => {
    await new Plant(plantMock).save();
    let plant = await Plant.findById(plantId);
    expect(plant).not.toBeNull();
    const response = await request(app)
      .put('/api/plants/update/' + plantId)
      .send({
        _id: plantId,
        name: 'name',
        wateringFrequency: 'wateringFrequency',
        light: 'light',
        thirsty: false,
      })
      .expect(200);
    expect(response.body.message).toBe('plant updated');
    let updatedPlant = await Plant.findById(plantId);
    expect(updatedPlant?.light).toEqual('light');
  });

  test('update plant error 500', async () => {
    await new Plant(plantMock).save();
    let plant = await Plant.findById(plantId);
    expect(plant).not.toBeNull();
    const response = await request(app)
      .put('/api/plants/update/' + plantId)
      .send({
        _id: wrongId,
        name: 1,
        wateringFrequency: 'wateringFrequency',
        light: 'light',
        thirsty: false,
      })
      .expect(500);
    expect(response.body.message).toBe('updating plant failed');
  });

  test('deletePlant success 200', async () => {
    await new Plant(plantMock).save();
    let plant = await Plant.findById(plantId);
    expect(plant).not.toBeNull();
    const plantCountBefore = await Plant.countDocuments();
    const response = await request(app)
      .delete('/api/plants/delete/' + plantId)
      .send()
      .expect(200);
    const plantCheck = await Plant.findById(plantId);
    expect(plantCheck).toBeNull();
    expect(response.body.message).toBe('plant deleted');
    const plantCountAfter = await Plant.countDocuments();
    expect(plantCountBefore - 1).toBe(plantCountAfter);
  });

  test('deletePlant error 500', async () => {
    jest.spyOn(container.resolve(PlantsRepository), 'deleteOne').mockRejectedValue(new Error());
    await new Plant(plantMock).save();
    let plant = await Plant.findById(plantId);
    expect(plant).not.toBeNull();
    const plantCountBefore = await Plant.countDocuments();
    const response = await request(app)
      .delete('/api/plants/delete/' + wrongId)
      .send()
      .expect(500);
    const plantCheck = await Plant.findById(plantId);
    expect(plantCheck).not.toBeNull();
    expect(response.body.message).toBe('deleting plant failed');
    const plantCountAfter = await Plant.countDocuments();
    expect(plantCountBefore).toBe(plantCountAfter);
    expect(container.resolve(PlantsRepository).deleteOne).toHaveBeenCalled();
  });

  test('getPlantById success 200', async () => {
    await new Plant(plantMock).save();
    const count = await Plant.countDocuments();
    expect(count).toBe(1);
    const response = await request(app)
      .get('/api/plants/getOne/' + plantId)
      .send()
      .expect(200);
    expect(response.body.message).toBe('plant fetched');
  });

  test('getPlantById error 500', async () => {
    await new Plant(plantMock).save();
    const count = await Plant.countDocuments();
    expect(count).toBe(1);
    jest.spyOn(container.resolve(PlantsRepository), 'findOne').mockRejectedValue(new Error());
    const response = await request(app)
      .get('/api/plants/getOne/' + plantId)
      .send()
      .expect(500);
    expect(response.body.message).toBe('fetching plant failed');
  });
});
