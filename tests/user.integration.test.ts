import app from '../src/app';
import mongoose from 'mongoose';
import request from 'supertest';
import User from '../src/models/user.model';
import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt';

describe('userModule', () => {
  const url = process.env.MONGO + '';

  const userId = new mongoose.Types.ObjectId();
  const userOne = {
    _id: userId,
    email: 'user@controller.com',
    verified: true,
    username: 'user',
    password: 'password',
    tokens: [
      {
        token: jwt.sign(
          {
            userId: userId,
            email: 'user@controller.com',
            username: 'user',
          },
          process.env.JWT_SECRET!,
        ),
      },
    ],
  };

  beforeAll(async () => {
    await mongoose.disconnect();
    await mongoose.connect(url, {});
    await User.deleteMany();
    await new User(userOne).save();
  });

  beforeEach(async () => {});

  afterAll(async () => {
    await User.deleteMany();
    await mongoose.disconnect();
  });

  test('signup success 201', async () => {
    const userCountBefore = await User.countDocuments();
    const response = await request(app)
      .post('/api/auth/signup')
      .send({
        email: 'fiehra@gsg.com',
        username: 'fiehra',
        password: 'fiehra',
      })
      .expect(201);
    expect(response.body.message).toBe('user created');
    expect(userCountBefore + 1).toBe(await User.countDocuments());
  });

  test('signup failed 401 username already exists', async () => {
    const userCountBefore = await User.countDocuments();
    const response = await request(app)
      .post('/api/auth/signup')
      .send({
        email: 'user@newEmail.com',
        username: 'user',
        password: 'user',
      })
      .expect(401);
    expect(response.body.message).toBe('username already exists');
    expect(userCountBefore).toBe(await User.countDocuments());
  });

  test('signup failed 400 email already taken', async () => {
    const userCountBefore = await User.countDocuments();

    const response = await request(app)
      .post('/api/auth/signup')
      .send({
        email: 'user@controller.com',
        username: 'newUser',
        password: 'user',
      })
      .expect(400);
    expect(response.body.message).toBe('email already taken');
    expect(userCountBefore).toBe(await User.countDocuments());
  });

  test('signup failed 500', async () => {
    jest.spyOn(bcrypt, 'hash').mockRejectedValueOnce(new Error());

    const response = await request(app)
      .post('/api/auth/signup')
      .send({
        username: 'controller',
        password: 'noob',
      })
      .expect(500);
    expect(response.body.message).toBe('signup failed');
    expect(bcrypt.hash).toHaveBeenCalled();
  });

  test('login success', async () => {
    jest.spyOn(bcrypt, 'compare').mockResolvedValueOnce(true);
    const fetchedUser = await User.findOne({ email: 'user@controller.com' });
    expect(fetchedUser!.username).toBe('user');

    const responseLogin = await request(app)
      .post('/api/auth/login')
      .send({
        username: 'user',
        password: 'password',
      })
      .expect(200);
    expect(responseLogin.body.authData.username).toBe('user');
    expect(responseLogin.body.message).toBe('login success');
    expect(bcrypt.compare).toHaveBeenCalled();
  });

  //   test('login not verified', async () => {
  //     const responseLogin = await request(app)
  //       .post('/api/auth/login')
  //       .send({
  //         username: 'fiehra',
  //         password: 'fiehra',
  //       })
  //       .expect(401);
  //     expect(responseLogin.body.message).toBe('not verified');
  //   });

  test('login failed wrong password', async () => {
    const fetchedUser = await User.findOne({ email: 'user@controller.com' });
    expect(fetchedUser?.username).toBe('user');

    const response = await request(app)
      .post('/api/auth/login')
      .send({
        username: 'user',
        password: 'wrongPassword',
      })
      .expect(401);
    expect(response.body.message).toBe('wrong credentials');
  });

  test('login failed user not found', async () => {
    const response = await request(app)
      .post('/api/auth/login')
      .send({
        username: 'canei',
        password: 'password',
      })
      .expect(404);
    expect(response.body.message).toBe('user not found');
  });

  test('login failed error 500', async () => {
    jest.spyOn(bcrypt, 'compare').mockRejectedValueOnce(new Error());
    const response = await request(app)
      .post('/api/auth/login')
      .send({
        username: 'user',
        password: 'password',
      })
      .expect(500);
    expect(response.body.message).toBe('login failed');
    expect(bcrypt.compare).toHaveBeenCalled();
  });
});
