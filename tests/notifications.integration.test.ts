import app from '../src/app';
import mongoose from 'mongoose';
import request from 'supertest';
import ScheduledNotification from '../src/models/scheduledNotification.model';

describe('notificationsModule', () => {
  const url = process.env.MONGO + '';

  const scheduledNotificationMock = {
    _id: new mongoose.Types.ObjectId(),
    registrationToken: 'token',
    time: new Date().setSeconds(new Date().getSeconds() + 2),
    notification: {
      title: 'title',
      body: 'body',
    },
  };

  beforeAll(async () => {
    await mongoose.connect(url, {});
  });

  beforeEach(async () => {
    await ScheduledNotification.deleteMany();
  });

  afterAll(async () => {
    await ScheduledNotification.deleteMany();
    await mongoose.disconnect();
  });

  test('sendPushNotification success 200', async () => {
    // const response = request(app)
    //   .post('/api/notifications/send')
    //   .send({
    //     registrationToken: 'token',
    //   })
    //   .expect(200);
  });
});
