import jwt, { JwtPayload } from 'jsonwebtoken';

export default function (req, res, next) {
  try {
    const token = req.headers.authorization.split(' ')[1];
    const decodedToken = jwt.verify(token, process.env.JWT_SECRET!) as JwtPayload;
    // add userData object to request
    req.userData = {
      email: decodedToken.email,
      userId: decodedToken.userId,
      username: decodedToken.username,
      role: decodedToken.role,
    };
    next();
  } catch (error) {
    return res.status(401).json({
      message: 'not authenticated',
    });
  }
}
