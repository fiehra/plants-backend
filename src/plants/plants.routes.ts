import express from 'express';
import { PlantsController } from './plants.controller';
import { autoInjectable } from 'tsyringe';

@autoInjectable()
export class PlantsRoutes {
  router = express.Router();

  constructor(private plantsController: PlantsController) {
    this.configureRoutes();
  }

  configureRoutes() {
    this.router.post('/plants/create', this.plantsController.create);
    this.router.get('/plants/getAll', this.plantsController.getAll);
    this.router.get('/plants/getOne/:id', this.plantsController.getOne);
    this.router.put('/plants/update/:id', this.plantsController.update);
    this.router.delete('/plants/delete/:id', this.plantsController.delete);
  }
}
