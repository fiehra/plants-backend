import { autoInjectable, container } from 'tsyringe';
import { PlantsService } from './plants.service';

@autoInjectable()
export class PlantsController {
  constructor() {}

  create(req, res, next) {
    container.resolve(PlantsService).createPlant(req, res);
  }

  getAll(req, res, next) {
    container.resolve(PlantsService).getAll(req, res);
  }

  getOne(req, res, next) {
    container.resolve(PlantsService).getPlantById(req, res);
  }

  update(req, res, next) {
    container.resolve(PlantsService).updatePlant(req, res);
  }

  delete(req, res, next) {
    container.resolve(PlantsService).deletePlant(req, res);
  }
}
