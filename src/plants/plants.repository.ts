import { autoInjectable } from 'tsyringe';
import Plant, { PlantInterface } from '../models/plant.model';

@autoInjectable()
export class PlantsRepository {
  save(plant: PlantInterface) {
    return Plant.create(plant);
  }

  async findAll() {
    // transform object so __v key is ommited
    return Plant.find().sort({ name: 'desc' }).select('-__v');
  }

  async findAllForUser(userId: String) {
    // transform object so __v key is ommited
    return Plant.find().select({ _id: userId });
  }

  async deleteOne(query: any) {
    return Plant.deleteOne(query);
  }

  async updateOne(query: any, plant: PlantInterface) {
    return Plant.updateOne(query, plant);
  }

  async findOne(query: any) {
    return Plant.findOne(query);
  }
}
