import { autoInjectable } from 'tsyringe';
import Plant from '../models/plant.model';
import { PlantsRepository } from './plants.repository';
import { Request, Response } from 'express';

@autoInjectable()
export class PlantsService {
  constructor(private plantsRepository: PlantsRepository) {}

  async createPlant(req: Request, res: Response) {
    try {
      const plant = new Plant({
        name: req.body.name,
        wateringFrequency: req.body.wateringFrequency,
        light: req.body.light,
        thirsty: req.body.thirsty,
      });
      const result = await this.plantsRepository.save(plant);
      return res.status(201).json({
        message: 'plant created',
        plantId: result._id,
      });
    } catch (error) {
      return res.status(500).json({
        message: 'creating plant failed',
      });
    }
  }

  async getAll(req: Request, res: Response) {
    try {
      const fetchedPlants = await this.plantsRepository.findAll();
      const count = await Plant.countDocuments();
      return res.status(200).json({
        message: 'plants fetched',
        plants: fetchedPlants,
        maxPlants: count,
      });
    } catch (error) {
      return res.status(500).json({
        message: 'fetching plants failed',
      });
    }
  }

  async getPlantById(req: Request, res: Response) {
    try {
      let fetchedPlant;
      fetchedPlant = await this.plantsRepository.findOne({ _id: req.params.id });
      return res.status(200).json({
        message: 'plant fetched',
        plant: fetchedPlant,
      });
    } catch (error) {
      return res.status(500).json({
        message: 'fetching plant failed',
      });
    }
  }

  async updatePlant(req: Request, res: Response) {
    try {
      const plant = new Plant({
        _id: req.body._id,
        name: req.body.name,
        wateringFrequency: req.body.wateringFrequency,
        light: req.body.light,
        thirsty: req.body.thirsty,
        lastWater: req.body.lastWater,
      });
      const result = await this.plantsRepository.updateOne({ _id: req.params.id }, plant);
      if (result.modifiedCount > 0) {
        return res.status(200).json({
          message: 'plant updated',
        });
      }
    } catch (error) {
      return res.status(500).json({
        message: 'updating plant failed',
      });
    }
  }

  async deletePlant(req: Request, res: Response) {
    try {
      await this.plantsRepository.deleteOne({ _id: req.params.id });
      return res.status(200).json({
        message: 'plant deleted',
      });
    } catch (error) {
      return res.status(500).json({
        message: 'deleting plant failed',
      });
    }
  }
}
