import { autoInjectable } from 'tsyringe';
import Device, { DeviceInterface } from '../models/device.model';
import ScheduleNotification, {
  ScheduledNotificationInterface,
} from '../models/scheduledNotification.model';

@autoInjectable()
export class NotificationsRepository {
  async saveDevice(device: DeviceInterface) {
    return Device.create(device);
  }

  async saveScheduledNotification(scheduledNotification: ScheduledNotificationInterface) {
    return ScheduleNotification.create(scheduledNotification);
  }

  async deleteScheduledNotification(query) {
    return ScheduleNotification.deleteOne(query);
  }

  async findAllScheduledNotifications() {
    return ScheduleNotification.find();
  }
}
