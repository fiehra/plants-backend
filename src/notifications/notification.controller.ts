import { autoInjectable, container } from 'tsyringe';
import { NotificationsService } from './notifications.service';

@autoInjectable()
export class NotificationsController {
  constructor(private notificationsService: NotificationsService) {}

  registerDevice(req, res) {}

  giveWaterNotification(req, res) {
    container.resolve(NotificationsService).giveWaterPushNotification(req, res);
  }

  clearExpired(req, res, next) {
    container.resolve(NotificationsService).clearExpiredNotifications();
  }

  // getAll(req, res, next) {
  //   NotificationsService.getAllDevices(req, res);
  // }

  // delete(req, res, next) {
  //   NotificationsService.deleteDevice(req, res);
  // }

  // findOne(req, res, next) {
  //   NotificationsService.getDeviceByRegistrationToken(req, res);
  // }
}
