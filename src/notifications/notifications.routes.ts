import express from 'express';
import { NotificationsController } from './notification.controller';
import { autoInjectable } from 'tsyringe';

@autoInjectable()
export class NotificationsRoutes {
  router = express.Router();

  constructor(private notificationsController: NotificationsController) {
    this.configureRoutes();
  }

  configureRoutes() {
    this.router.post('/notifications/register', this.notificationsController.registerDevice);
    this.router.post(
      '/notifications/giveWater',
      this.notificationsController.giveWaterNotification,
    );
  }
}
