import admin from '../../firebase-config';
import schedule from 'node-schedule';
import ScheduledNotification from '../models/scheduledNotification.model';
import { DateHelper } from '../helper/date.helper';
import { PlantsRepository } from '../plants/plants.repository';
import { NotificationsRepository } from './notifications.repository';
import { autoInjectable } from 'tsyringe';
import { Request, Response } from 'express';
import Device, { DeviceInterface } from '../models/device.model';
import mongoose from 'mongoose';

@autoInjectable()
export class NotificationsService {
  notification_options = {
    priority: 'high',
    timeToLive: 0,
  };

  constructor(
    private plantsRepository: PlantsRepository,
    private notificationsRepository: NotificationsRepository,
    private dateHelper: DateHelper,
  ) {}

  async giveWaterPushNotification(req: Request, res: Response) {
    try {
      const message = {
        notification: {
          title: req.body.notificationTitle,
          body: req.body.notificationBody,
        },
      };
      const timeTillPush = this.dateHelper.calcTimeToNextWater(req.body.wateringFrequency);
      let notificationTime = new Date();
      // notificationTime.setDate(notificationTime.getDate() + timeTillPush);
      notificationTime.setSeconds(notificationTime.getSeconds() + timeTillPush);

      const scheduledNotification = new ScheduledNotification({
        registrationToken: req.body.registrationToken,
        time: notificationTime,
        notification: message.notification,
      });

      // fetch plant and update last water value
      const fetchedPlant = await this.plantsRepository.findOne({ _id: req.body.plantId });
      if (fetchedPlant) {
        fetchedPlant.lastWater = req.body.lastWater;
        await this.plantsRepository.updateOne({ _id: req.body.plantId }, fetchedPlant);
      }

      if (scheduledNotification.registrationToken == '') {
        return;
      } else {
        // fetch plant and update thirsty once the scheduled job executes
        schedule.scheduleJob(notificationTime, async () => {
          const plant = await this.plantsRepository.findOne({ _id: req.body.plantId });
          if (plant) {
            plant.thirsty = true;
            await this.plantsRepository.updateOne({ _id: req.body.plantId }, plant);
          }
          admin
            .messaging()
            .sendToDevice(req.body.registrationToken, message, this.notification_options);
        });
      }

      await this.notificationsRepository.saveScheduledNotification(scheduledNotification);
      return res.status(200).json({
        message: 'notification scheduled successfully',
      });
    } catch (error) {
      return res.status(500).json({
        message: 'scheduling or sending notification failed',
      });
    }
  }

  async clearExpiredNotifications() {
    let scheduledNotifications = await this.notificationsRepository.findAllScheduledNotifications();
    scheduledNotifications.forEach(async (notification) => {
      if (new Date(notification.time) < new Date()) {
        await this.notificationsRepository.deleteScheduledNotification(notification);
      }
    });
  }

  async rescheduleNotifications() {
    let scheduledNotifications = await this.notificationsRepository.findAllScheduledNotifications();
    scheduledNotifications.forEach((scheduleNotification) => {
      schedule.scheduleJob(scheduleNotification.time, () => {
        admin
          .messaging()
          .sendToDevice(
            scheduleNotification.registrationToken,
            { notification: scheduleNotification.notification },
            this.notification_options,
          );
      });
    });
  }

  registerDevice(req: Request, res: Response) {
    let newDevice: DeviceInterface = new Device({
      _id: new mongoose.Types.ObjectId(),
      registrationToken: req.body.registrationToken,
      userId: req.body.userId,
    });
    this.notificationsRepository
      .saveDevice(newDevice)
      .then((result) => {
        return res.status(201).json({
          message: 'device registered',
          plant: result,
        });
      })
      .catch((error) => {
        return res.status(500).json({
          error: error,
          message: 'registering device failed',
        });
      });
  }
}
