import mongoose from 'mongoose';
import app from './app';

const normalizePort = (val) => {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    return val;
  }
  if (port >= 0) {
    return port;
  }
  return false;
};

const onError = (error) => {
  if (error.syscall !== 'listen') {
    throw error;
  }
  const bind = typeof port === 'string' ? 'pipe ' + port : 'port ' + port;
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
    default:
      throw error;
  }
};

const url = process.env.MONGO + '';
mongoose
  .connect(url, {})
  .then(() => {
    console.log('connected to plantsDB on ' + new Date().toDateString());
  })
  .catch((error) => {
    console.log(error);
    console.log('connection failed');
  });

const port = normalizePort(process.env.PORT);

if (process.env.NODE_ENV !== 'test') {
  app.on('error', onError);
  app.listen(port, () => {
    console.log(`node server is listening on port ${process.env.PORT}`);
  });
}
