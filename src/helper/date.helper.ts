import { autoInjectable } from 'tsyringe';

@autoInjectable()
export class DateHelper {
  constructor() {}

  addDays(date, days) {
    const result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
  }

  calcTimeToNextWater(wateringFrequency: String): number {
    if (wateringFrequency === '1 day') {
      return 1;
    } else if (wateringFrequency === '3 days') {
      return 3;
    } else if (wateringFrequency === '7 days') {
      return 7;
    } else if (wateringFrequency === '14 days') {
      return 14;
    } else if (wateringFrequency === '28 days') {
      return 30;
    } else {
      return 1;
    }
  }

  calcExpirationDate(date: Date, duration: number): Date {
    let expirationDate = new Date(date);
    expirationDate.setSeconds(expirationDate.getSeconds() + duration);
    return expirationDate;
  }
}
