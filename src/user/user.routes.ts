import express from 'express';
import { UserController } from './user.controller';
import { autoInjectable } from 'tsyringe';

@autoInjectable()
export class UserRoutes {
  router = express.Router();

  constructor(private userController: UserController) {
    this.configureRoutes();
  }

  configureRoutes() {
    this.router.post('/authentication/signup', this.userController.signup);
    this.router.post('/authentication/login', this.userController.login);
  }
}
