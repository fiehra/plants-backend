import { autoInjectable } from 'tsyringe';
import User, { UserInterface } from '../models/user.model';

@autoInjectable()
export class UserRepository {
  async save(user: UserInterface) {
    return User.create(user);
  }

  async findOne(query: any) {
    return User.findOne(query);
  }

  // async updateOne(query: any, user: UserInterface) {
  //   return User.updateOne(query, user);
  // }
}
