import mongoose, { Schema, model } from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';

export interface DeviceInterface {
  _id: mongoose.Types.ObjectId;
  registrationToken: string;
  userId: string;
}

const deviceSchema = new Schema<DeviceInterface>({
  _id: { type: mongoose.Schema.Types.ObjectId },
  registrationToken: { type: String, required: true },
  userId: { type: String, required: true },
});

deviceSchema.plugin(uniqueValidator);
export default model('Device', deviceSchema);
