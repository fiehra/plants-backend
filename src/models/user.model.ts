import mongoose, { Schema, model } from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';

export interface UserInterface {
  _id: mongoose.Types.ObjectId;
  email: string;
  username: string;
  password: string;
  role: string;
  //   verified: boolean;
}

const userSchema = new Schema<UserInterface>({
  _id: { type: mongoose.Schema.Types.ObjectId },
  email: { type: String, unique: true },
  username: { type: String, unique: true },
  password: { type: String, required: true },
  role: { type: String, default: 'user' },
  //   verified: { type: Boolean, default: false },
});

userSchema.plugin(uniqueValidator);
export default model('User', userSchema);
