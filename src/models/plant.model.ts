import { Schema, model } from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';

export interface PlantInterface {
  id: String;
  name: String;
  wateringFrequency: String;
  light: String;
  thirsty: Boolean;
  lastWater: String;
}

const plantSchema = new Schema<PlantInterface>({
  id: String,
  name: { type: String, required: true },
  wateringFrequency: { type: String, required: true },
  light: { type: String, required: true },
  thirsty: { type: Boolean, default: true },
  lastWater: { type: String, default: '' },
});

plantSchema.plugin(uniqueValidator);
export default model('Plant', plantSchema);
