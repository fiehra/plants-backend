import { Schema, model } from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';

export interface ScheduledNotificationInterface {
  id: String;
  registrationToken: String;
  time: Date;
  notification: {
    title: String;
    body: String;
  };
}

const scheduledNotificationSchema = new Schema<ScheduledNotificationInterface>({
  id: String,
  time: { type: Date, required: true },
  registrationToken: { type: String, required: true },
  notification: { type: {}, required: true },
});

scheduledNotificationSchema.plugin(uniqueValidator);
export default model('ScheduledNotification', scheduledNotificationSchema);
