import * as dotenv from 'dotenv';
dotenv.config();
import 'reflect-metadata';

import express from 'express';
import compression from 'compression';
import bodyParser from 'body-parser';
import helmet from 'helmet';
import { container } from 'tsyringe';
import { PlantsRoutes } from './plants/plants.routes';
import { NotificationsRoutes } from './notifications/notifications.routes';
import { NotificationsService } from './notifications/notifications.service';
import { UserRoutes } from './user/user.routes';

const app = express();

app.use(compression());
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: false,
  }),
);
app.use(helmet());

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept, Authorization',
  );
  res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE');
  next();
});

container.resolve(NotificationsService).clearExpiredNotifications();
container.resolve(NotificationsService).rescheduleNotifications();

app.use('/api', container.resolve(PlantsRoutes).router);
app.use('/api', container.resolve(NotificationsRoutes).router);
app.use('/api', container.resolve(UserRoutes).router);

export default app;
