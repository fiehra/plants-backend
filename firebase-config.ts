var admin = require('firebase-admin');
var serviceAccount = require('./plants-push-firebase-admin.json');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
});

export default admin;
