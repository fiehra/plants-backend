project setup:

// yarn

development:

1. git clone https://gitlab.com/fiehra/plants-backend.git or clone with ssh

2. yarn install

3. add dev.env in src directory

// dev.env template:

PORT=XXXX <br>
MONGO=mongo-connection <br>
JWT_SECRET=yourJWTSecretKey <br>
NODE_ENV=environment <br>

4. yarn dev

test:

1. add test.env
2. yarn test
